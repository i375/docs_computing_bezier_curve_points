
/**
 * Basic 2D point class.
 */
class Point2D
{
    x:number
    y:number
    
    constructor(x:number, y:number)
    {
        this.x = x
        this.y = y   
    }
}

function bezComputer(c1:Point2D, c2:Point2D, c3:Point2D, c4:Point2D, t:number):Point2D
{    
    var i1 = lerp2D(c1, c2, t)
    var i2 = lerp2D(c2, c3, t)
    var i3 = lerp2D(c3, c4, t)
    
    var i4 = lerp2D(i1, i2, t)
    var i5 = lerp2D(i2, i3, t)
    
    var bezPoint = lerp2D(i4, i5, t)
    
    return bezPoint
}

/**
 * Function for computing linear interpolation from p1 to p2.
 * 
 */
function lerp2D(p1:Point2D, p2:Point2D, t:number):Point2D
{
    var interpolatedPoint = new Point2D(0.0,0.0)
    
    interpolatedPoint.x = (1-t)*p1.x + t*p2.x
    interpolatedPoint.y = (1-t)*p1.y + t*p2.y
    
    return interpolatedPoint
}